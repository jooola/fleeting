package connector

import (
	"context"
	"net"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/fleeting/fleeting/provider"
)

func TestConnectionFailed(t *testing.T) {
	listener, err := net.Listen("tcp", ":0")
	require.NoError(t, err)

	defer listener.Close()

	t.Parallel()

	t.Run("reach deadline", func(t *testing.T) {
		_, err := DialSSH(context.Background(), provider.ConnectInfo{
			ConnectorConfig: provider.ConnectorConfig{
				Timeout: time.Second,
			},
			InternalAddr: listener.Addr().String(),
		}, DialOptions{})

		require.ErrorContains(t, err, "ssh: handshake failed: read tcp")
	})

	t.Run("context deadline exceeded", func(t *testing.T) {
		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()

		_, err := DialSSH(ctx, provider.ConnectInfo{
			ConnectorConfig: provider.ConnectorConfig{
				Timeout: time.Hour,
			},
			InternalAddr: listener.Addr().String(),
		}, DialOptions{})

		require.ErrorIs(t, err, context.DeadlineExceeded)
	})
}
