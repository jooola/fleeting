package proxy

import (
	"compress/gzip"
	"embed"
	"fmt"
	"io"
)

//go:generate env GOOS=windows GOARCH=386 go build -trimpath -ldflags "-s -w" -o dist/windows-386.exe ./cmd/
//go:generate gzip --no-name -f -9 dist/windows-386.exe

//go:embed dist/windows-386.exe.gz
var f embed.FS

//go:embed dist/check-tunnel-binary.ps1
var PSCheckTunnelBinary string

//go:embed dist/deploy-tunnel-binary.ps1
var PSDeployTunnelBinary string

func ReadBinary(goos, goarch string) []byte {
	// When the GOOS is windows, we always send the 386 binary, because this
	// should have compatibility with both amd64 and arm (via emulation).
	if goos == "windows" {
		goarch = "386"
	}

	name := fmt.Sprintf("dist/%s-%s", goos, goarch)
	if goos == "windows" {
		name += ".exe"
	}

	fi, err := f.Open(name + ".gz")
	if err != nil {
		return nil
	}

	r, err := gzip.NewReader(fi)
	if err != nil {
		return nil
	}

	buf, err := io.ReadAll(r)
	if err != nil {
		return nil
	}

	return buf
}
