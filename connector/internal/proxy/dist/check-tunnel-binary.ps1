$version = '%s'

if (test-path -path fleeting-proxy-${version}.exe) {
    exit 0
}

exit 1