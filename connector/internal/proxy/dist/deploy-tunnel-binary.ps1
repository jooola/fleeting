$version = '%s';

if (test-path -path fleeting-proxy-${version}.exe) {
	exit 0
}

$name = [System.IO.Path]::GetTempFileName()
[System.IO.File]::WriteAllBytes($name, [System.Convert]::FromBase64String('%s'))

Move-Item -path $name -destination .\fleeting-proxy-${version}.exe