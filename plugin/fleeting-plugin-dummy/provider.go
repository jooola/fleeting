package dummy

import (
	"context"
	"fmt"
	"sync"

	"github.com/hashicorp/go-hclog"

	"gitlab.com/gitlab-org/fleeting/fleeting/provider"
)

var _ provider.InstanceGroup = (*InstanceGroup)(nil)

type InstanceGroup struct {
	MaxSize      int
	ManualDelete bool
	ListenSSH    bool

	SimulateDeleting bool // simulating moving "deleting" instances to "deleted"

	mu        sync.Mutex
	instances map[string]*Instance
	settings  provider.Settings

	logger hclog.Logger

	sshAddr     string
	sshShutdown func()
}

type Instance struct {
	ID    string
	State provider.State
}

func (g *InstanceGroup) Init(_ context.Context, logger hclog.Logger, settings provider.Settings) (provider.ProviderInfo, error) {
	g.logger = logger

	g.init()
	g.settings = settings

	if g.ListenSSH {
		shutdown, addr, err := sshServe(logger)
		if err != nil {
			return provider.ProviderInfo{}, err
		}
		g.sshAddr = addr
		g.sshShutdown = shutdown
	}

	return provider.ProviderInfo{
		ID:      "dummy/dummy",
		MaxSize: g.MaxSize,
		Version: "dummy version",
	}, nil
}

func (g *InstanceGroup) Update(_ context.Context, update func(id string, state provider.State)) error {
	g.mu.Lock()
	defer g.mu.Unlock()

	for idx, inst := range g.instances {
		update(inst.ID, inst.State)

		if g.SimulateDeleting && inst.State == provider.StateDeleting {
			// set to deleted on next update
			g.instances[idx].State = provider.StateDeleted
		}
	}

	return nil
}

func (g *InstanceGroup) Increase(_ context.Context, delta int) (int, error) {
	for i := 0; i < delta; i++ {
		g.AddInstance(provider.StateCreating)
	}

	return delta, nil
}

func (g *InstanceGroup) Decrease(_ context.Context, instances []string) ([]string, error) {
	g.mu.Lock()
	defer g.mu.Unlock()

	success := make([]string, 0, len(instances))

	for _, id := range instances {
		inst, ok := g.instances[id]
		if !ok {
			return success, fmt.Errorf("instance (%q) not found", id)
		}

		if !g.ManualDelete {
			inst.State = provider.StateDeleting
		}
		success = append(success, id)
	}

	return success, nil
}

func (g *InstanceGroup) ConnectInfo(ctx context.Context, instance string) (provider.ConnectInfo, error) {
	g.mu.Lock()
	defer g.mu.Unlock()

	_, ok := g.instances[instance]
	if !ok {
		return provider.ConnectInfo{}, fmt.Errorf("instance %s does not exist", instance)
	}

	return provider.ConnectInfo{
		ConnectorConfig: g.settings.ConnectorConfig,
		ExternalAddr:    g.sshAddr,
		InternalAddr:    g.sshAddr,
	}, nil
}

func (g *InstanceGroup) Shutdown(ctx context.Context) error {
	if g.sshShutdown != nil {
		g.sshShutdown()
	}

	g.logger.Info("dummy shutdown called")
	return nil
}
