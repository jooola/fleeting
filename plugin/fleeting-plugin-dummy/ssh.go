package dummy

import (
	"context"
	"crypto/rand"
	"crypto/rsa"
	"errors"
	"fmt"
	"net"

	"github.com/hashicorp/go-hclog"
	"golang.org/x/crypto/ssh"
)

func sshServe(logger hclog.Logger) (func(), string, error) {
	listener, err := net.Listen("tcp", "127.0.0.1:0")
	if err != nil {
		return nil, "", err
	}
	logger.Info("ssh listening", "addr", listener.Addr().String())

	key, err := rsa.GenerateKey(rand.Reader, 4096)
	if err != nil {
		return nil, "", fmt.Errorf("generating private key: %w", err)
	}

	signer, err := ssh.NewSignerFromKey(key)
	if err != nil {
		return nil, "", fmt.Errorf("signer from key: %w", err)
	}

	config := &ssh.ServerConfig{
		PasswordCallback: func(c ssh.ConnMetadata, pass []byte) (*ssh.Permissions, error) {
			return nil, nil
		},
	}
	config.AddHostKey(signer)

	ctx, cancel := context.WithCancel(context.Background())

	go func() {
		defer listener.Close()

		for {
			lconn, err := listener.Accept()
			if err != nil {
				if errors.Is(err, net.ErrClosed) {
					return
				}

				panic(err)
			}

			if ctx.Err() != nil {
				return
			}

			logger.Info("ssh accepted connection", "local addr", lconn.LocalAddr().String(), "remote addr", lconn.RemoteAddr().String())

			conn, chans, reqs, err := ssh.NewServerConn(lconn, config)
			if err != nil {
				panic(err)
			}

			go ssh.DiscardRequests(reqs)

			go func() {
				defer conn.Close()

				for newChannel := range chans {
					if ctx.Err() != nil {
						return
					}

					channel, reqs, err := newChannel.Accept()
					if err != nil {
						logger.Error("ssh accepting channel", "err", err)
						return
					}

					logger.Info("ssh accepted channel")

					for req := range reqs {
						logger.Info("ssh got request", "type", req.Type, "payload", string(req.Payload), "reply", req.WantReply)
						if req.WantReply {
							req.Reply(true, []byte{})
						}

						channel.Write([]byte("hello world"))
						channel.CloseWrite()
						channel.SendRequest("exit-status", false, []byte{0, 0, 0, 0})
						channel.Close()
					}
				}
			}()
		}
	}()

	return func() {
		listener.Close()
		cancel()
	}, listener.Addr().String(), nil
}
