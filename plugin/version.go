package plugin

import (
	"fmt"
	"runtime"
)

type VersionInfo struct {
	Name      string
	Version   string
	Revision  string
	Reference string
	BuiltAt   string
}

func (v *VersionInfo) String() string {
	return v.Version
}

func (v *VersionInfo) BuildInfo() string {
	return fmt.Sprintf(
		"sha=%s; ref=%s; go=%s; built_at=%s; os_arch=%s/%s",
		v.Revision,
		v.Reference,
		runtime.Version(),
		v.BuiltAt,
		runtime.GOOS,
		runtime.GOARCH,
	)
}

func (v *VersionInfo) Full() string {
	version := fmt.Sprintf("Name:         %s\n", v.Name)
	version += fmt.Sprintf("Version:      %s\n", v.Version)
	version += fmt.Sprintf("Git revision: %s\n", v.Revision)
	version += fmt.Sprintf("Git ref:      %s\n", v.Reference)
	version += fmt.Sprintf("GO version:   %s\n", runtime.Version())
	version += fmt.Sprintf("Built:        %s\n", v.BuiltAt)
	version += fmt.Sprintf("OS/Arch:      %s/%s\n", runtime.GOOS, runtime.GOARCH)

	return version
}
